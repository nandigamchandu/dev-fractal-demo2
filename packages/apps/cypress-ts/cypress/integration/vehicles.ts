describe('Testing vehicle page', () => {
  it('check url', () => {
      cy.visit('/vehicles')
      cy.url().should('equal', 'http://localhost:1234/vehicles')
  })

  context('check all elements in vehicle page', ()=>{
    it('check elements in side bar', () => {
      cy.get('.menu')
        .get('.menu-list')
        .children()
        .should('have.length', 3)

      cy.get('.menu')
        .get('.menu-list')
        .children()
        .eq(2).contains(/clients/i)

      cy.get('.menu')
        .get('.menu-list')
        .children()
        .get('.is-active')
        .debug()
        .contains(/vehicles/i)
    })

    it('check all buttons', ()=>{
      cy.get('.button').contains(/add vehicle/i)
      cy.queryAllByText(/assign/i).should('have.length', 54)
      cy.queryByText(/next/i).should('be.visible')
      cy.queryByText(/previous/i).should('be.hidden')
    })

    it('check table', ()=>{

      cy.get('tr').get('th').should('have.length', 6)
      cy.get('tr').should('have.length', 55)
      cy.get('tr').get('th').contains(/vehicle name/i)
      cy.get('tr')
        .get('th')
        .contains(/registration number/i)
      cy.get('tr')
        .get('th')
        .contains(/insurance expiry/i)
      cy.get('tr')
        .get('th')
        .contains(/last service/i)
      cy.get('tr')
        .get('th')
        .contains(/status/i)
      cy.get('tr')
        .get('th')
        .contains(/actions/i)

    })

  })

  context('vehicle add form', ()=>{
    it('check all elements',()=>{
      cy.queryByText(/add vehicle/i).click()
      cy.url().should('equal', 'http://localhost:1234/vehicles/new')
      cy.get('input[name=vehicleSerialNum]').should('have.attr', 'type', 'text')
      cy.get('input[name=makersClass]').should('have.attr', 'type', 'text')
      cy.get('input[name=vehicleClass]').should('have.attr', 'type', 'text')
      cy.get('input[name=manufactureYear]').should('have.attr', 'type', 'text')
      cy.get('input[name=color]').should('have.attr', 'type', 'text')
      cy.get('input[name=registrationNumber]').should(
        'have.attr',
        'type',
        'text',
      )
      cy.get('input[name=warrantyExpiry]').should('have.attr', 'type', 'text')
      cy.get('input[name=lastService]').should('have.attr', 'type', 'text')
      cy.get('input[name=insuranceExpiry]').should('have.attr', 'type', 'text')
      cy.queryByText(/save/i).should('be.visible')
      cy.queryByText(/reset/i).should('be.disabled')
    })


    it('check all required fields ', () => {
      cy.queryByText(/save/i).click()
      cy.get('input[name=vehicleSerialNum]')
        .parent()
        .next()
        .contains(/this is a required field/i)
      cy.get('input[name=makersClass]')
        .parent()
        .next()
        .contains(/this is a required field/i)
      cy.get('input[name=vehicleClass]')
        .parent()
        .next()
        .contains(/this is a required field/i)
      cy.get('input[name=color]')
        .parent()
        .next()
        .contains(/this is a required field/i)
      cy.get('input[name=registrationNumber]')
        .parent()
        .next()
        .contains(/this is a required field/i)
    })

    it('check reset', () => {
      cy.get('input[name=vehicleSerialNum]').type('asdhsagd')
      cy.get('input[name=makersClass]').type('jhgdsjf')
      cy.get('input[name=vehicleClass]').type('dkfhsdf')
      cy.get('input[name=manufactureYear]')
        .click()
        .get('.react-datepicker__day')
        .eq(6)
        .click()
      cy.get('input[name=color]').type('pink')
      cy.get('input[name=registrationNumber]').type('fjskdhfjksdhf')
      cy.get('input[name=warrantyExpiry]')
        .click()
        .get('.react-datepicker__day')
        .last()
        .click()
      cy.get('input[name=lastService]')
        .click()
        .get('.react-datepicker__day')
        .last()
        .click()
      cy.get('input[name=insuranceExpiry]')
        .click()
        .get('.react-datepicker__day')
        .last()
        .click()
      cy.queryByText(/reset/i).click()

      cy.get('input[name=vehicleSerialNum]').should('have.attr', 'value', '')
      cy.get('input[name=makersClass]').should('have.attr', 'value', '')
      cy.get('input[name=vehicleClass]').should('have.attr', 'value', '')
      cy.get('input[name=manufactureYear]').should('have.attr', 'value', Cypress.moment().format('DD/MM/YYYY'))
      cy.get('input[name=color]').should('have.attr', 'value', '')
      cy.get('input[name=registrationNumber]').should('have.attr', 'value', '')
      cy.get('input[name=warrantyExpiry]').should(
         'have.attr',
         'value',
         Cypress.moment().format('DD/MM/YYYY'),
       )
      cy.get('input[name=lastService]').should(
        'have.attr',
        'value',
        Cypress.moment().format('DD/MM/YYYY'),
      )
       cy.get('input[name=insuranceExpiry]').should(
         'have.attr',
         'value',
         Cypress.moment().format('DD/MM/YYYY'),
       )
    })

    it('check submit', () => {
      cy.get('input[name=vehicleSerialNum]').type('asdhsagd')
      cy.get('input[name=makersClass]').type('jhgdsjf')
      cy.get('input[name=vehicleClass]').type('dkfhsdf')
      cy.get('input[name=manufactureYear]')
        .click()
        .get('.react-datepicker__day')
        .eq(6)
        .click()
      cy.get('input[name=color]').type('pink')
      cy.get('input[name=registrationNumber]').type('fjskdhfjksdhf')
      cy.get('input[name=warrantyExpiry]')
        .click()
        .get('.react-datepicker__day')
        .last()
        .click()
      cy.get('input[name=lastService]')
        .click()
        .get('.react-datepicker__day')
        .last()
        .click()
      cy.get('input[name=insuranceExpiry]')
        .click()
        .get('.react-datepicker__day')
        .last()
        .click()
      cy.queryByText(/save/i).click()

    })
    it('check response after submit', () => {
     //  check response after submit
    })  
  })

})
