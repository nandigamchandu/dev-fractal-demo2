let logData : string

before (()=>{
    cy.request('POST', 'http://localhost:3000/v1/session', {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    }).its('body').then((res)=>{
        logData = JSON.stringify(res)
    })
})

beforeEach(()=>{
    cy.visit('/drivers', {
        onBeforeLoad(win){
            win.localStorage.setItem('logData', logData)
        }
    })
})

describe('Testing vehicle list data', () => {
  it('should pass', () => {
     cy.request({
       url: 'http://localhost:3000/v1/vehicles',
       headers: {
         Authorization: `bearer ${JSON.parse(logData).data.token}`,
       },
     })
       .its('body').its('data').its('count').should('eq', 91)
  })
})