import { rest, toJSONServerQuery } from '@stp/devfractal'
import { ObjC, Props } from '@stp/utils'
import { fakeBaseURL } from '../config'
import {
  AssignDriver,
  AssignVehicle,
  Client,
  Driver,
  Vehicle,
} from './models'

// tslint:disable-next-line: typedef
function api<Opt extends Props, Req extends Props>(
  spec: ObjC<Opt, Req>,
  resource: string,
) {
  return rest(spec, 'id', resource, { baseURL: fakeBaseURL }, toJSONServerQuery)
}

export const driverAPI = api(Driver, 'drivers')
export const vehicleAPI = api(Vehicle, 'vehicles')
export const clientAPI = api(Client, 'clients')
export const assignDriverAPI = api(AssignDriver, 'assignDriver')
export const assignVehicleAPI = api(AssignVehicle, 'assignVehicle')
