import {
  Get,
  http as httpAPI,
  paths,
  Post,
  Put,
  Route,
  useParams,
} from '@stp/devfractal'
import { string, type } from '@stp/utils'
import React from 'react'
import {
  AuthUserInfo,
  VehicleAdd as VA,
  vehicleAPI,
  VehicleData,
  VehicleEdit as VE,
   VehicleResponse
} from '../common'
import { VehicleForm, VehicleList } from '../views'

const ps = paths(vehicleAPI.resource)
const baseURL: string = 'http://localhost:3000/v1'

export const isAuthData = (logData: unknown): logData is AuthUserInfo => {
  const userData = (logData as AuthUserInfo)
  return userData.data.token !== undefined
}

const getToken = (logData : unknown): string =>isAuthData(logData) ? logData.data.token : ''

const token: string =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6Ijc0MzkxNmY5LWE3N2EtNDNjMC1hMDgyLTkwZmNmOTQ5MmEzMCIsInJvbGUiOiJzdXBlcl9hZG1pbiIsImlhdCI6MTU4MDcwNzE4MiwiZXhwIjoxNTgwNzEwNzgyfQ.uI-hGe2MzUp5RWAUYBI1R5TkkSif3K6PocXcYZCWVXI'
export const cargosUrl = () => {
    const http = httpAPI({
      baseURL,
      headers: {
        Authorization: `bearer ${token}`,
      },
    })
    return http
  }

async function getVehicleList(): Promise<readonly VehicleData[]> {
  try {
    const vehicles = await cargosUrl().get(
      { resource: 'vehicles' },
      VehicleResponse,
    )
    return vehicles.data.rows
  } catch (error) {
    throw Error(error)
  }
}

async function getVehicle(
  id: string,
): Promise<VE['data']> {
  try {
    const vehicles = await cargosUrl().get(
      { resource: 'vehicles', path: id },
      VE,
    )
    return vehicles.data
  } catch (error) {
    throw Error(error)
  }
}

async function putVehicle(
  data: VehicleData,
): Promise<VE['data']> {
  const { vehicleName, ...rest } = data
  try {
    const vehicles = await cargosUrl().put({ resource: 'vehicles' }, rest, VE)
    return vehicles.data
  } catch (error) {
    throw Error(error)
  }
}

async function postVehicle(
  data: VA,
): Promise<VE['data']> {
  try {
    const vehicles = await cargosUrl().post({ resource: 'vehicles' }, data, VE)
    return vehicles.data
  } catch (error) {
    throw Error(error)
  }
}

export const deleteList = async (
  url: string,
) => {
  try {
    await cargosUrl().del(url)
    return
  } catch (error) {
    throw Error(error)
  }
}

const VehicleListRoute = () => {
  return (
    <Get
      asyncFn={getVehicleList}
      component={VehicleList}
    />
  )
}

const VehicleAdd = () => (
  <Post
    redirectTo={ps.list}
    component={VehicleForm}
    onPost={postVehicle}
  />
)

const VehicleEdit = () => {
  const params  = useParams(type({ [vehicleAPI.idKey]: string }))
  return (
    <Put
      id={params[vehicleAPI.idKey as string] as any}
      doGet={id => getVehicle(id as string)}
      onPut={(_id, data) => putVehicle(data)}
      component={VehicleForm}
      redirectTo={ps.list}
    />
  )
}

export const VehicleRoutes = () => {
  return (
    <>
      <Route
        path={ps.create}
        render={() => <VehicleAdd  />}
      />
      <Route
        path={ps.list}
        render={() => (
          <VehicleListRoute
          />
        )}
      />
      <Route
        path={ps.edit}
        render={() => <VehicleEdit />}
      />
    </>
  )
}

// export const VehicleRoutes2 = () => (
//   <CrudRoutes api={vehicleAPI} form={VehicleForm} list={VehicleList} />
// )
