import { Column, Columns, Router, SimpleRedirect } from '@stp/devfractal'
import 'bulma/css/bulma.css'
import React from 'react'
import { SideMenuView, Visibility } from './components'
import {
  ClientRoutes,
  DriverRoutes,
  VehicleRoutes,
} from './pages'
import './stylesheets/styles.scss'
import { AssignDriverRoute } from './views'
import { AssignVehicleRoute } from './views/AssignVehicle'

export const SuperAdmin = () => {
  const [visibility, setVisibility] = React.useState<Visibility>('full')
  const handleClick = () => {
    setVisibility(visibility === 'full' ? 'minimal' : 'full')
  }

  return (
    <Router variant="browser">
      <Columns>
        <SideMenuView visibility={visibility} onClick={handleClick} />
        <Column>
          <SimpleRedirect from="/" to="/drivers" />
          <DriverRoutes />
          <AssignDriverRoute />
          <ClientRoutes />
          <VehicleRoutes />
          <AssignVehicleRoute />
        </Column>
      </Columns>
    </Router>
  )
}
