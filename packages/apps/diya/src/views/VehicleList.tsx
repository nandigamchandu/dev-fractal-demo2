import {
  CreateLink,
  links,
  RoutedPager,
  Section,
} from '@stp/devfractal'
import React from 'react'
import { VehicleData} from '../common'
import { HeadTitle } from '../components'
import { DiyaTable } from '../components/DiyaTable'
import {deleteList} from '../pages/VehicleRoutes'

const vehicleLinks = links('vehicles')


export const VehicleList = ({
  data,
}: {
  readonly data: readonly VehicleData[]
}) => {
  return (
    <Section>
      <HeadTitle>Vehicles</HeadTitle>

      <CreateLink alignment="right" variant="primary" to={vehicleLinks.create}>
        Add Vehicle
      </CreateLink>

      <DiyaTable
        data={data}
        select={[
          'vehicleName',
          'registrationNumber',
          'insuranceExpiry',
          'lastService',
          'status',
        ]}
        editTo={v => vehicleLinks.edit(v.id)}
        assignTo={v => `/assignVehicle/${v.id}`}
        onDelete={v => {
          // tslint:disable-next-line: no-floating-promises
          deleteList(`/vehicles/${v.id}`)
      }}
      />
      <RoutedPager />
    </Section>
  )
}